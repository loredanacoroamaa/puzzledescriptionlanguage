from nltk import *
from nltk.sem.drt import DrtParser
from nltk.sem import logic
logic._counter._value = 0

from nltk.sem import Expression
read_expr = Expression.fromstring
p1 = read_expr('man(socrates)')
p2 = read_expr('all x.(man(x) -> mortal(x))')
c  = read_expr('mortal(socrates)')
prover = Prover9() 
prover.config_prover9('/Users/loredanacristinacoroama/Downloads/LADR-2009-11A/bin')
prover.prove(c, [p1,p2])
ResolutionProver().prove(c, [p1,p2], verbose=True)


prover = ResolutionProverCommand(c, [p1,p2])
print(prover.prove())


prover = Prover9() 
Prover9().config_prover9('/Users/loredanacristinacoroama/Downloads/LADR-2009-11A/bin')

dt = DiscourseTester(['a boxer walks', 'every boxer chases a girl'])
dt.sentences()
dt.grammar()
dt.readings()
